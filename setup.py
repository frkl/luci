#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""The setup script."""

from setuptools import setup, find_packages

with open("README.rst") as readme_file:
    readme = readme_file.read()

with open("HISTORY.rst") as history_file:
    history = history_file.read()

requirements = [
    "frkl==1.0.0b1",
]

setup_requirements = ["pytest-runner"]

test_requirements = ["pytest==3.6.0"]

setup(
    author="Markus Binsteiner",
    author_email="makkus@posteo.de",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Developers",
        "License :: Other/Proprietary License",
        "Natural Language :: English",
        "Programming Language :: Python :: 2",
        "Programming Language :: Python :: 2.7",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.4",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
    ],
    description="luci - a generic metadata interpreter",
    entry_points={
        "console_scripts": ["luci=luci.cli:cli"],
        "luci.lucifiers": [
            "metadata=luci.lucifiers:MetadataLucifier",
            "template=luci.lucifiers:TemplateLucifier",
            "download=luci.lucifiers:DownloadLucifier",
            "debug=luci.lucifiers:DebugLucifier",
            "cat=luci.lucifiers:CatLucifier",
            # 'cli=luci.cli_lucifier:CliLucifier'
        ],
        "luci.indexes": [
            # "folder=lupkg.lupkg_index:LupkgFilesInFolderIndex",
            "folder=luci.luitem_index:LuItemFolderIndex",
            "metadata-folder=luci.lupkg_index:LupkgMetadataFolderIndex",
            "multi=luic.luitem_index:LuItemMultiIndex",
            "file=luci.luitem_index:LuItemFileIndex",
        ],
        "luci.dictlet_readers": [
            "yaml=luci.readers:LucifyYamlDictionaryDictletReader",
            "yaml-folder=luci.readers:MetadataFolderReader",
            "class=luci.readers:ClassDictletReader",
        ],
        "luci.dictlet_finders": [
            "path=luci.finders:PathDictletFinder",
            "folder=luci.finders.FolderFinder",
        ],
    },
    install_requires=requirements,
    license="Parity Public License 5.0.0",
    long_description=readme + "\n\n" + history,
    include_package_data=True,
    keywords="luci",
    name="luci",
    packages=find_packages(include=["luci"]),
    setup_requires=setup_requirements,
    test_suite="tests",
    tests_require=test_requirements,
    url="https://gitlab.com/frkl/luci",
    version="1.0.0b1",
    zip_safe=False,
)
