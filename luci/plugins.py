# -*- coding: utf-8 -*-

# python 3 compatibility
from __future__ import absolute_import, division, print_function, unicode_literals

import logging
import sys

from stevedore import driver, extension

log = logging.getLogger("lucify")


# extensions
# ------------------------------------------------------------------------
def load_dictlet_finder_extension(name, init_params=None):
    """Loading a dictlet finder extension.

    Args:
      name (str): the registered name of the extension
      init_params (dict): the parameters to initialize the extension object

    Returns:
      DictletFinder: the extension object
    """

    if init_params is None:
        init_params = {}
    log2 = logging.getLogger("stevedore")
    out_hdlr = logging.StreamHandler(sys.stdout)
    out_hdlr.setFormatter(
        logging.Formatter("dictlet_finder plugin error -> %(message)s")
    )
    out_hdlr.setLevel(logging.DEBUG)
    log2.addHandler(out_hdlr)
    log2.setLevel(logging.INFO)

    log.debug("Loading dictlet_finder...")

    mgr = driver.DriverManager(
        namespace="luci.dictlet_finders",
        name=name,
        invoke_on_load=True,
        invoke_kwds=init_params,
    )

    log.debug(
        "Registered dictlet_finder: {}".format(
            ", ".join(ext.name for ext in mgr.extensions)
        )
    )

    return mgr.driver


def load_dictlet_reader_extension(name, init_params=None):
    """Loading a processor extension.

    Args:
      name (str): the registered name of the extension

    Returns:
      DictletReader: the extension object
    """

    log2 = logging.getLogger("stevedore")
    out_hdlr = logging.StreamHandler(sys.stdout)
    out_hdlr.setFormatter(
        logging.Formatter("dictlet_reader plugin error -> %(message)s")
    )
    out_hdlr.setLevel(logging.DEBUG)
    log2.addHandler(out_hdlr)
    log2.setLevel(logging.INFO)

    log.debug("Loading dictlet_reader...")

    mgr = driver.DriverManager(
        namespace="luci.dictlet_readers",
        name=name,
        invoke_on_load=True,
        invoke_kwds=init_params,
    )

    log.debug(
        "Registered dictlet_reader: {}".format(
            ", ".join(ext.name for ext in mgr.extensions)
        )
    )

    return mgr.driver


def get_lucifier_extensions():
    """Loading all extensions.

    """

    log2 = logging.getLogger("stevedore")
    out_hdlr = logging.StreamHandler(sys.stdout)
    out_hdlr.setFormatter(logging.Formatter("lucifier plugin error -> %(message)s"))
    out_hdlr.setLevel(logging.DEBUG)
    log2.addHandler(out_hdlr)
    log2.setLevel(logging.INFO)
    # log2.setLevel(logging.DEBUG)

    log.debug("Loading lucifiers...")

    mgr = extension.ExtensionManager(
        namespace="luci.lucifiers",
        invoke_on_load=False,
        # invoke_args=(parsed_args.width,),
    )
    # mgr = stevedore.driver.DriverManager(
    # namespace='luci.lucifiers',
    # name=name,
    # invoke_on_load=True,
    # invoke_args=(init_params,))

    # log.debug("Registered plugins: {}".format(", ".join(
    # ext.name for ext in mgr.extensions)))

    return mgr


def get_plugin_names():
    """Get all plugin names.
    """

    return lucifier_extension_mgr.entry_points_names()


def get_plugin_details():
    """Get plugin details."""

    result = {}
    for ext in lucifier_extension_mgr.extensions:
        details = {"name": ext.name, "class": ext.plugin}

        result[ext.name] = details

    return result


lucifier_extension_mgr = get_lucifier_extensions()
extension_details = get_plugin_details()
