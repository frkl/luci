# -*- coding: utf-8 -*-
"""Top-level package for luci."""

__author__ = """Markus Binsteiner"""
__email__ = "makkus@posteo.de"
__version__ = "1.0.0b1"

# flake8: noqa

from . import param_types

vars_file = param_types.VarsFileType()
