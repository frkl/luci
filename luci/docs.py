from .cli import LuciCommand


def get_lucifier(command_name):
    """Utility method to generate a LucifierCommand instance for the automated docs generation with Sphinx."""

    return LuciCommand().get_command(None, command_name)


debug = get_lucifier("debug")
template = get_lucifier("template")
cat = get_lucifier("cat")
download = get_lucifier("download")
metadata = get_lucifier("metadata")
