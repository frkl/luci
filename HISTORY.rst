*******
History
*******

0.2
===

* under development

0.1.1 (2018-04-05)
==================

* Refactored basically everything
* Much more documentation

0.1.0 (2018-03-15)
==================

* First release on PyPI.
