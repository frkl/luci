****
luci
****

.. image:: https://img.shields.io/pypi/v/luci.svg
           :target: https://pypi.python.org/pypi/luci
           :alt: pypi

.. image:: https://readthedocs.org/projects/luci/badge/?version=latest
           :target: https://docs.luci.frkl.io/en/latest/?badge=latest
           :alt: documentation

.. image:: https://gitlab.com/frkl/luci/badges/develop/pipeline.svg
           :target: https://gitlab.com/frkl/luci/pipelines
           :alt: pipeline status

.. image:: https://pyup.io/repos/github/makkus/luci/shield.svg
           :target: https://pyup.io/repos/github/makkus/luci/
           :alt: updates

.. image:: https://coveralls.io/repos/github/makkus/luci/badge.svg?branch=develop
           :target: https://coveralls.io/github/makkus/luci?branch=develop
           :alt: coveralls

.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
           :target: https://github.com/ambv/black
           :alt: codestyle


NOTE: this README is a bit out of date, will update as soon as I find some time

What ``luci`` is about:

- you add metadata to any type of text file (most likely, but not necessarily, in it's comments)
- you point ``luci`` to that file
- ``luci`` reads the metadata
- ``luci`` uses that metadata as configuration to (optionally) create a command-line interface for the file in question, and as configuration for a task of your choosing
- ``luci`` executes that task, letting the user specify additional metadata/configuration values via the dynamically created command-line interface (if applicable)

'What.. ??', 'Those words you just said are weird!!', 'English much?!?' -- Yes, sorry. I'm having real trouble coming up with a good and simple explanation for all this. Honestly, I'd be happy to settle for only one of the two!

``luci`` is the result of one of those situations where I needed a solution for an issue I had (and which would be even more difficult to explain), and where I decided I saw an interesting enough pattern worth trying to implement a generic solution for. I know, I know, typical over-engineered piece of crap solution looking for problems. I don't care though.

To illustrate the class of problems *luci* can help with, here are two quick and dirty...

Examples
========

A dynamic text emitter
----------------------

Say you are developing a shell script that processes text via ``stdin``, and for development purposes (testing regular expressions?) you need to pipe in some (changing, but predictable) text input. This can be achieved quickly in a number of ways, but with *luci* I think it's especially straightforward. You create a file (let's call it ``dev_input``):

.. code-block::

   # __luci__:
   #   vars:
   #     - he
   #     - she
   #
   What he said: {{ he }}
   Then, she said: {{ she }}

Now, make it executable (``chmod +x dev_input``), and put it somewhere into your ``$PATH``. That's all, now you've got an executable that emits templated text (which can be way more complex than this example, you'd just use some of the more advanced bells and whistles of the jinja2_ templating engine) that can be controlled via command-line switches:

.. code-block::

    ❯  luci template dev_input --help
    Usage: luci template dev_input [OPTIONS]

    Options:
      --he TEXT   n/a
      --she TEXT  n/a
      --help      Show this message and exit.

    ❯  luci template dev_input --he Cool --she "Really?"
    What he said: Cool
    Then, she said: Really?

Check out the collected `examples <https://docs.luci.frkl.io/en/latest/examples.html#super-basic-example>`_ for more details and options.

Make a file 'updateable'
------------------------

Say we want to share the file from the last example with our co-workers, and every now and then we update it to add new 'features'. Of course, we will check it into a git repository. But checking out that repo might or might not be too much overhead for just one file. We could, instead, add the download url to the file's metadata, and use a *luci* task to update it. Here's how the updated file looks like:

.. code-block::

   # __luci__:
   #   vars:
   #     - he
   #     - she
   #     - name:
   #         default: she
   #
   # url: https://raw.githubusercontent.com/makkus/luci-examples/master/download/dev-input/dev_input
   #
   What he said: {{ he }}
   Then, {{ name }} said: {{ she }}

Once our co-worker has this file on their machine, and we publish an update, all they need to do is:

.. code-block:: console

   ❯ luci download /home/markus/.local/bin/dev_input --replace

Again, this is only a very simplistic example to quickly show what *luci* is about. Check out the `full example <https://docs.luci.frkl.io/en/latest/examples.html#include-update-information-into-a-shell-script>`_ (and the `luci documentation <https://docs.luci.frkl.io>`_ in general) for more details.



*luci* comes with a `few default 'commands' <https://docs.luci.frkl.io/en/latest/usage.html>`_ ('*lucifiers*' in luci-speak) like the `download <https://docs.luci.frkl.io/en/latest/lucifiers/download.html>`_ one. You can easily write your own *lucifiers*, and that's where I see *luci*'s real value, as that let's you, fairly quickly, create applications and scripts for sorta 'secondary' tasks related to the data you commonly work with. In an organized and reproducible way.

Because of it's generic nature I think examples are best suited to convey the type of problems *luci* can help solve. Which is why I wrote up some more `usage examples <https://luci.readthedocs.io/en/latest/examples.html>`_.

Obviously, nothing *luci* does can't be achieved by writing your own scripts. I honestly have no idea how people feel about embedding task-specific metadata in their files. I think it's a neat idea in some cases, even if I find it really hard to tell you why I do :-)


Install
=======

.. code-block:: console

   ❯ pip install luci

I'll leave it as exercise for the reader to decide whether to use ``sudo`` or create a virtualenv or similar for a tidier installation.

Features
========

* generic (to a fault)
* extensible (plugin-architecture, add your own `dictlet-finders <https://luci.readthedocs.org/en/latest/dictlet_finders.html>`_, `dictlet-readers <https://luci.readthedocs.org/en/latest/dictlet_readers.html>`_, and `lucifiers <https://luci.readthedocs.org/en/latest/lucifiers.html>`_)
* dynamic command-line interface generation
* includes default task implementations ('*lucifiers*') that help with development/debugging (``debug``, ``metadata``, ``cat``) as well as common generic use-cases (``template``, ``download``)

ToDo
====

* look into performance
* add more (input) formats in addition to *yaml*
* add a few more generic *lucifiers*
* enable the use of non-text files, directories, urls etc.
* tests

Links
=====

* Documentation: https://docs.luci.frkl.io.

License
-------

Parity Public License 3.0.0

Please check the `LICENSE <LICENSE>`_ file in this repository (it's a short license!), https://freckles.io/licensing (not written yet) and the `README.rst file <contributing/README.rst>`_ in the ``contributing`` folder.


Credits
=======

click_
    used to dynamically create the dictlet command-line interface

jinja2_
    does all the templating in the background

stevedore_
    used to manage plugins

cookiecutter_
    was used to create the initial project layout

.. _click: http://click.pocoo.org/6/
.. _jinja2: http://jinja.pocoo.org
.. _stevedore: https://docs.openstack.org/stevedore/latest/
.. _cookiecutter: https://github.com/audreyr/cookiecutter
.. _jq: https://stedolan.github.io/jq/
